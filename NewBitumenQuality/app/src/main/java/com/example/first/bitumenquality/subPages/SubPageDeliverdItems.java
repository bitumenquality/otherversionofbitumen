package com.example.first.bitumenquality.subPages;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.first.bitumenquality.ListViewAdapter;
import com.example.first.bitumenquality.MainActivity;
import com.example.first.bitumenquality.R;
import com.example.first.bitumenquality.dataAccessObject.HerstellerDAO;
import com.example.first.bitumenquality.dataAccessObject.LieferungDAO;
import com.example.first.bitumenquality.dataAccessObject.SorteDAO;
import com.example.first.bitumenquality.databaseClasses.Hersteller;
import com.example.first.bitumenquality.databaseClasses.Lieferung;
import com.example.first.bitumenquality.databaseClasses.Sorte;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class SubPageDeliverdItems extends Fragment {

    private LieferungDAO dataLieferung ;
    private HerstellerDAO dataHersteller = new HerstellerDAO(getContext());
    private SorteDAO dataSource6 = new SorteDAO(getContext());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_page_deliverd_items, container, false);

        showLieferungListEntries(rootView);
        makeSpinnerLieferung(rootView);
        activateAddButtonLieferung(rootView);
        initializeContextualActionBarLieferung(rootView);

        return rootView;
    }

    private void makeSpinnerLieferung(View rootView) {

        Spinner My_spinner_Sample = (Spinner) rootView.findViewById(R.id.spinner_deliverditems);
        ArrayAdapter<String> my_Adapter_Sample = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item, getTableValuesLieferung());
        my_Adapter_Sample.add("None selected !");
        my_Adapter_Sample.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        My_spinner_Sample.setAdapter(my_Adapter_Sample);

    }

    public ArrayList<String> getTableValuesLieferung() {
        ArrayList<String> my_array = new ArrayList<String>();
        try {
            dataHersteller.open();

            List<Hersteller> list = dataHersteller.getAllHersteller();


            for (int i = 0; i < list.size(); i++) {
                dataSource6.open();
                String SorteName = "";
                List<Sorte> lists = dataSource6.getAllSorte();
                for (int j = 0; j < lists.size(); j++) {
                    if (lists.get(j).getId() == list.get(i).getSortenId()) {
                        SorteName = lists.get(j).getBezeichnung();
                        String NAME = SorteName + "/" + list.get(i).getName();
                        my_array.add(NAME);
                        break;
                    }
                }

            }

            dataSource6.close();

            dataHersteller.close();

        } catch (Exception e) {
            Toast.makeText(getActivity().getApplicationContext(), "Error encountered.",
                    Toast.LENGTH_LONG);
        }
        return my_array;
    }

    private void activateAddButtonLieferung(final View rootview) {


        Button buttonAddDelivery = (Button) rootview.findViewById(R.id.btn_deliverditems_save);
        final EditText editTextInfo = (EditText) rootview.findViewById(R.id.et_deliverditems_beschreibung);
        final EditText editTextName = (EditText) rootview.findViewById(R.id.et_deliverditems_bezeichnung);
        final Spinner editTextLHersteller = (Spinner) rootview.findViewById(R.id.spinner_deliverditems);

        final EditText editTextDay = (EditText) rootview.findViewById(R.id.et_day_deliverd);
        final EditText editTextMonth = (EditText) rootview.findViewById(R.id.et_month_deliverd);
        final EditText editTextYear = (EditText) rootview.findViewById(R.id.et_year_deliverd);

        buttonAddDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String day = editTextDay.getText().toString();
                String month = editTextMonth.getText().toString();
                String year = editTextYear.getText().toString();

                String name = editTextName.getText().toString();
                String info = editTextInfo.getText().toString();
                String lieferungHersteller = editTextLHersteller.getSelectedItem().toString();
                String[] segs = lieferungHersteller.split("/");


                if (TextUtils.isEmpty(day)) {
                    editTextDay.setError(getString(R.string.output_errorMessage));
                    return;
                }

                if (TextUtils.isEmpty(month)) {
                    editTextMonth.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(year)) {
                    editTextYear.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(name)) {
                    editTextName.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(info)) {
                    editTextInfo.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (editTextLHersteller.getSelectedItem().toString().equals("None selected !")) {
                    TextView errorText = (TextView) editTextLHersteller.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("no one is selected");//changes the selected item text to this
                    return;
                }

                boolean dayBool = TextUtils.isDigitsOnly(editTextDay.getText());
                boolean monthBool = TextUtils.isDigitsOnly(editTextMonth.getText());
                boolean yearBool = TextUtils.isDigitsOnly(editTextYear.getText());

                if (!dayBool || Integer.parseInt(day) < 1 || Integer.parseInt(day) > 31) {
                    editTextDay.setError("value problem!");
                    return;
                }
                if (!monthBool || Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {
                    editTextMonth.setError("value problem!");
                    return;
                }
                if (!yearBool || Integer.parseInt(year) < 1000 || Integer.parseInt(year) > 3000) {
                    editTextYear.setError("value problem!");
                    return;
                }

                dataHersteller.open();
                dataLieferung.open();

                Log.d("MainActivity", "onClick: hersteller name:" + segs[0]);
                Long id = dataHersteller.getAllHersteller(segs[1]).get(0).getId();

                String newDate = String.format("%04d-%02d-%02d", Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
                Log.d("mbmb", newDate);
                Date dTemp = Date.valueOf(newDate);


                Log.d("MainActivity", "onClick: date:" + dTemp.toString());
                dataLieferung.createLieferung(id, dTemp, name, info);
                dataLieferung.close();
                dataHersteller.close();

                InputMethodManager inputMethodManager;
                inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                showLieferungListEntries(rootview);

            }
        });
    }

    private void showLieferungListEntriesLieferung(View rootview) {

        List<Lieferung> memoList = dataLieferung.getAllLieferung();

        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

        dataHersteller = new HerstellerDAO(getContext());
        dataHersteller.open();
        List<Hersteller> list = dataHersteller.getAllHersteller();
        dataHersteller.close();

        dataSource6 = new SorteDAO(getContext());
        dataSource6.open();
        List<Sorte> list2 = dataSource6.getAllSorte();
        dataSource6.close();

        String temp = "Empty !!!";

        String herstellerName = "";
        String SorteName = "";

        for (int i = 0; i < memoList.size(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            Lieferung lieferung = memoList.get(i);

            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getId() == lieferung.getHerstllerId()) {
                    herstellerName = list.get(j).getName();
                    for (int k = 0; k < list2.size(); k++) {
                        if (list2.get(k).getId() == list.get(j).getSortenId()) {
                            SorteName = list2.get(j).getBezeichnung();
                            temp = " [ " + SorteName + " - " + herstellerName + " ]";
                            break;
                        }
                    }

                }
            }
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            map.put("FIRST_COLUMN", " " + temp);
            map.put("SECOND_COLUMN", " " + lieferung.getBezeichnung());
            map.put("THIRD_COLUMN", " " + lieferung.getBeschreibung());
            map.put("FOURTH_COLUMN", " " + df.format(lieferung.getDate()));
            mylist.add(map);
        }

        //TODO make an base adapter

        ListView memosListView = (ListView) rootview.findViewById(R.id.lv_deliverditems);
        ListViewAdapter adapter = new ListViewAdapter(getActivity(), mylist);
        memosListView.setAdapter(adapter);

    }

    private void showLieferungListEntries(View rootView) {

        dataLieferung = new LieferungDAO(getContext());
        dataLieferung.open();
        showLieferungListEntriesLieferung(rootView);
        dataLieferung.close();
    }

    private void initializeContextualActionBarLieferung(final View rootview) {

        final ListView listView = (ListView) rootview.findViewById(R.id.lv_deliverditems);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                AppCompatActivity a = (AppCompatActivity) getActivity();
                a.getMenuInflater().inflate(R.menu.menu_contextual_action_bar, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.cab_delete:
                        SparseBooleanArray touchedShoppingMemosPositions = listView.getCheckedItemPositions();
                        for (int i = 0; i < touchedShoppingMemosPositions.size(); i++) {
                            boolean isChecked = touchedShoppingMemosPositions.valueAt(i);
                            if (isChecked) {
                                int postitionInListView = touchedShoppingMemosPositions.keyAt(i);
                                HashMap<Integer, Object> temp = (HashMap<Integer, Object>) listView.getItemAtPosition(postitionInListView);
                                dataLieferung.open();
                                dataLieferung.deleteLieferung(dataLieferung.getAllLieferung().get(postitionInListView));
                                dataLieferung.close();

                            }
                        }
                        showLieferungListEntries(rootview);

                        mode.finish();
                        return true;

                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            makeSpinnerLieferung(getView());
        }
    }

}
