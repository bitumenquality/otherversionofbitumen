package com.example.first.bitumenquality;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.first.bitumenquality.subPages.SubPageDeliverdItems;
import com.example.first.bitumenquality.subPages.SubPageSampleProperties;
import com.example.first.bitumenquality.subPages.SubPageScanning;
import com.example.first.bitumenquality.subPages.SubPageScannedData;
import com.example.first.bitumenquality.subPages.SubPageSortType;
import com.example.first.bitumenquality.subPages.SubpageProvider;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";
    private SectionPageAdapter mSectionPageAdapter;
    public static SectionPageAdapter adapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Hide bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        mSectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        setUpViewPager(mViewPager);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Intent intent = new Intent(this, MyService.class);
        startService(intent);

    }

    public void setUpViewPager(ViewPager viewPager) {
        adapter = new SectionPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new SubPageScanning(), "SCANNING");
        adapter.addFragment(new SubPageScannedData(), "SCANNED DATA");
        adapter.addFragment(new SubPageSampleProperties(), "SAMPLE PROPERTIES");
        adapter.addFragment(new SubPageSortType(), "SORT TAPE");
        adapter.addFragment(new SubpageProvider(), "PROVIDER");
        adapter.addFragment(new SubPageDeliverdItems(), "DELIVERD ITEMS");
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
    }

}
