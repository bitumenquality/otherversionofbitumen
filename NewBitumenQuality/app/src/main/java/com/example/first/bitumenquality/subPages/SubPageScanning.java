package com.example.first.bitumenquality.subPages;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import pl.pawelkleczkowski.customgauge.CustomGauge;

import com.example.first.bitumenquality.MainActivity;
import com.example.first.bitumenquality.MyService;
import com.example.first.bitumenquality.R;
import com.example.first.bitumenquality.dataAccessObject.AlterungszustandDAO;
import com.example.first.bitumenquality.dataAccessObject.HerstellerDAO;
import com.example.first.bitumenquality.dataAccessObject.LieferungDAO;
import com.example.first.bitumenquality.dataAccessObject.ProbeDAO;
import com.example.first.bitumenquality.dataAccessObject.SorteDAO;
import com.example.first.bitumenquality.databaseClasses.Hersteller;
import com.example.first.bitumenquality.databaseClasses.Lieferung;
import com.example.first.bitumenquality.databaseClasses.Probe;
import com.example.first.bitumenquality.databaseClasses.Sorte;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import static android.content.Context.INPUT_METHOD_SERVICE;

public class SubPageScanning extends Fragment {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    //Für GPS notwendige Variablen

    private FusedLocationProviderClient mFusedLocationClient;
    Location mlocation;
    final Criteria criteria = new Criteria();

    //View for sevice
    ProgressBar progressBar;

    private MyBroadcastReceiver1 myBroadcastReceiver1;
    private MyBroadcastReceiver2 myBroadcastReceiver2;
    private MyBroadcastReceiver_Update myBroadcastReceiver_Update;

    TextView resultService;
    CustomGauge gauge1;
    CustomGauge gauge2;

    TextView resultQ1;
    TextView resultQ2;

    TextView info;

    IntentFilter intentFilter1;
    IntentFilter intentFilter2;
    IntentFilter intentFilter_update;
    Spinner My_spinner;

    String update = "";
    String msg1 = "";
    int value1;
    int value2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //GPS
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_sub_page_scanning, container, false);

        makeSpinnerScanning(rootView);
        activateAddButton(rootView);


        resultQ1 = rootView.findViewById(R.id.tv_scann_get_q1);
        resultQ2 = rootView.findViewById(R.id.tv_scann_get_q2);
        info = rootView.findViewById(R.id.tv_scann_disconnect);

        gauge1 = rootView.findViewById(R.id.gauge1_scann);
        gauge2 = rootView.findViewById(R.id.gauge2_scann);
        gauge1.setValue(1);
        gauge2.setValue(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationSave();

        resultService = rootView.findViewById(R.id.tv_scann_info);

        myBroadcastReceiver_Update = new MyBroadcastReceiver_Update(getTargetFragment());
        intentFilter_update = new IntentFilter(MyService.ACTION_MyUpdate);
        intentFilter_update.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(myBroadcastReceiver_Update, intentFilter_update);

        myBroadcastReceiver1 = new MyBroadcastReceiver1(getTargetFragment());
        myBroadcastReceiver2 = new MyBroadcastReceiver2(getTargetFragment());


        //register BroadcastReceiver
        intentFilter1 = new IntentFilter(MyService.ACTION_MyIntentService1);
        intentFilter1.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(myBroadcastReceiver1, intentFilter1);

        intentFilter2 = new IntentFilter(MyService.ACTION_MyIntentService2);
        intentFilter2.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(myBroadcastReceiver2, intentFilter2);


        return rootView;
    }


    private void setGaugeColor(int q1, int q2) {
        Spinner editTextSample = getView().findViewById(R.id.spinner_scann);
        if (editTextSample.getSelectedItem().toString().equals("None selected !")) {
            TextView errorText = (TextView) editTextSample.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("no one is selected");//changes the selected item text to this
            gauge1.setValue(0);
            gauge1.setPointStartColor(Color.rgb(220, 220, 220));
            gauge1.setPointEndColor(Color.rgb(220, 220, 220));
            gauge2.setValue(0);
            gauge2.setPointStartColor(Color.rgb(220, 220, 220));
            gauge2.setPointEndColor(Color.rgb(220, 220, 220));
            return;
        }

        String name = editTextSample.getSelectedItem().toString();

        ProbeDAO dataProbe = new ProbeDAO(getContext());
        dataProbe.open();
        List<Probe> list = dataProbe.getAllProbe(name);
        dataProbe.close();

        LieferungDAO dataLieferung = new LieferungDAO(getContext());
        dataLieferung.open();
        List<Lieferung> listlieferung = dataLieferung.getAllLieferung();
        long herstellerid = 0;
        dataLieferung.close();

        for (int i = 0; i < listlieferung.size(); i++) {
            if (listlieferung.get(i).getId() == list.get(0).getLieferungId()) {
                herstellerid = listlieferung.get(i).getHerstllerId();
            }
        }

        HerstellerDAO dataHersteller = new HerstellerDAO(getContext());
        dataHersteller.open();
        List<Hersteller> listHersteller = dataHersteller.getAllHersteller();
        long sorteid = 0;

        for (int i = 0; i < listHersteller.size(); i++) {
            if (listHersteller.get(i).getId() == herstellerid) {
                sorteid = listHersteller.get(i).getSortenId();
            }
        }
        dataHersteller.close();

        SorteDAO dataSource6 = new SorteDAO(getContext());
        dataSource6.open();
        Sorte tempS = null;
        List<Sorte> listSorte = dataSource6.getAllSorte();
        for (int i = 0; i < listSorte.size(); i++) {
            if (listSorte.get(i).getId() == sorteid) {
                tempS = listSorte.get(i);
            }
        }
        dataSource6.close();

        if (tempS != null) {

            int q1minyellow = tempS.getQ1Min();
            int q1mingreen = tempS.getQ1MinGreenQ();
            int q2minyellow = tempS.getQ2Min();
            int q2mingreen = tempS.getQ2MinGreenQ();


            if (q1 == 0) {
                gauge1.getPointStartColor();
                gauge1.setPointStartColor(Color.rgb(220, 220, 220));
                gauge1.setPointEndColor(Color.rgb(220, 220, 220));
            } else if (q1 > q1mingreen) {
                gauge1.getPointStartColor();
                gauge1.setPointStartColor(Color.rgb(34, 139, 34));
                gauge1.setPointEndColor(Color.rgb(34, 139, 34));
            } else if (q1 > q1minyellow) {
                gauge1.getPointStartColor();
                gauge1.setPointStartColor(Color.rgb(255, 255, 0));
                gauge1.setPointEndColor(Color.rgb(255, 255, 0));
            } else {
                gauge1.getPointStartColor();
                gauge1.setPointStartColor(Color.rgb(255, 0, 0));
                gauge1.setPointEndColor(Color.rgb(255, 0, 0));
            }


            if (q2 == 0) {
                gauge2.getPointStartColor();
                gauge2.setPointStartColor(Color.rgb(220, 220, 220));
                gauge2.setPointEndColor(Color.rgb(220, 220, 220));
            } else if (q2 > q2mingreen) {
                gauge2.getPointStartColor();
                gauge2.setPointStartColor(Color.rgb(34, 139, 34));
                gauge2.setPointEndColor(Color.rgb(34, 139, 34));
            } else if (q2 > q2minyellow) {
                gauge2.getPointStartColor();
                gauge2.setPointStartColor(Color.rgb(255, 255, 0));
                gauge2.setPointEndColor(Color.rgb(255, 255, 0));
            } else {
                gauge2.getPointStartColor();
                gauge2.setPointStartColor(Color.rgb(255, 0, 0));
                gauge2.setPointEndColor(Color.rgb(255, 0, 0));
            }

            gauge1.setValue(q1);
            gauge2.setValue(q2);

        }

    }


    private void makeSpinnerScanning(View rootView) {

        My_spinner = rootView.findViewById(R.id.spinner_scann);
        ArrayAdapter<String> my_Adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item, getTableValuesProbe());
        my_Adapter.add("None selected !");
        my_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        My_spinner.setAdapter(my_Adapter);

    }


    private void activateAddButton(final View rootView) {


        final Spinner editTextSample = (Spinner) rootView.findViewById(R.id.spinner_scann);
        final EditText editTextScann_InternID = (EditText) rootView.findViewById(R.id.et_intern_number_id_scann);
        final EditText editTextScann_Name = (EditText) rootView.findViewById(R.id.actv_name_scann);
        final EditText editTextScann_Info = (EditText) rootView.findViewById(R.id.et_description_scann);

        final TextView ed_messungsfaktoren = (TextView) rootView.findViewById(R.id.tv_scann_disconnect);
        final TextView ed_messung1 = (TextView) rootView.findViewById(R.id.tv_scann_get_q1);
        final TextView ed_messung2 = (TextView) rootView.findViewById(R.id.tv_scann_get_q2);
        final Activity me = getActivity();

        Button buttonAddScann = (Button) rootView.findViewById(R.id.btn_scann_save);
        Log.d(LOG_TAG, "save button initialized");


        buttonAddScann.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(LOG_TAG, "save button clicked");

                String internID = editTextScann_InternID.getText().toString();
                String name = editTextScann_Name.getText().toString();
                String info = editTextScann_Info.getText().toString();

                String txt = "geo: " + mlocation.getLatitude() + "," + mlocation.getLongitude() +
                        "?q=" + mlocation.getLatitude() + "," + mlocation.getLongitude();

                Spinner editTextSample = getView().findViewById(R.id.spinner_scann);
                if (editTextSample.getSelectedItem().toString().equals("None selected !")) {
                    TextView errorText = (TextView) editTextSample.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("no one is selected");//changes the selected item text to this
                    gauge1.setValue(0);
                    gauge1.setValue(0);
                    return;
                }


                if (TextUtils.isEmpty(internID)) {
                    editTextScann_InternID.setError(getString(R.string.output_errorMessage));
                    return;
                }

                if (TextUtils.isEmpty(name)) {
                    editTextScann_Name.setError(getString(R.string.output_errorMessage));
                    return;
                }

                if (TextUtils.isEmpty(info)) {
                    editTextScann_Info.setError(getString(R.string.output_errorMessage));
                    return;
                }
                Calendar currenttime = Calendar.getInstance();
                Date sqldate = new Date((currenttime.getTime()).getTime());

                AlterungszustandDAO dataAlterungszustand = new AlterungszustandDAO(getContext());
                dataAlterungszustand.open();
                dataAlterungszustand.createAlterungszustand
                        (0, sqldate, internID + " " + name + " " + info, txt,
                                ed_messung1.getText().toString() + "-" + ed_messung2.getText().toString());
                dataAlterungszustand.close();


                InputMethodManager inputMethodManager;
                inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                Toast.makeText(getContext(), "Scanned data Saved", Toast.LENGTH_LONG).show();


            }
        });

    }

    public ArrayList<String> getTableValuesProbe() {

        ProbeDAO dataProbe = new ProbeDAO(getContext());
        ArrayList<String> my_array = new ArrayList<String>();
        try {
            dataProbe.open();

            List<Probe> list = dataProbe.getAllProbe();

            for (int i = 0; i < list.size(); i++) {
                String NAME = list.get(i).getBezeichnung();
                my_array.add(NAME);
            }
            dataProbe.close();

        } catch (Exception e) {
            Toast.makeText(getContext().getApplicationContext(), "Error encountered.",
                    Toast.LENGTH_LONG);
        }
        return my_array;
    }


    public void locationSave() {
        final LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mlocation = location;
                Log.d("Location Changes", location.toString());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("Status Changed", String.valueOf(status));
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("Provider Enabled", provider);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("Provider Disabled", provider);
            }
        };


        // Now create a location manager
        final LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // This is the Best And IMPORTANT part
        final Looper looper = null;
        int permissionAccessLocationFine = ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionAccessLocationCoarse = ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION);


        if (permissionAccessLocationFine != PackageManager.PERMISSION_GRANTED
                && permissionAccessLocationCoarse != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d(LOG_TAG, "permission coarse: " + permissionAccessLocationCoarse +
                    "; fine: " + permissionAccessLocationFine + "; parent: " + getActivity());

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        locationManager.requestSingleUpdate(criteria, locationListener, looper);

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {


                if (location != null) {
                    mlocation = location;
                }

            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        //un-register BroadcastReceiver
        getActivity().unregisterReceiver(myBroadcastReceiver_Update);
        getActivity().unregisterReceiver(myBroadcastReceiver1);
        getActivity().unregisterReceiver(myBroadcastReceiver2);


        Spinner editTextSample = getView().findViewById(R.id.spinner_scann);

        SharedPreferences prefs = getContext().getSharedPreferences("prefs_name", Context.MODE_PRIVATE);
        prefs.edit().putInt("spinner_indx", editTextSample.getSelectedItemPosition()).apply();

    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myBroadcastReceiver_Update, intentFilter_update);
        getActivity().registerReceiver(myBroadcastReceiver1, intentFilter1);
        getActivity().registerReceiver(myBroadcastReceiver2, intentFilter2);
        if (update.equals("Connected")) {
            info.setBackgroundResource(R.drawable.rounded_edittext5);
            info.setText("Connected");
            if (value1 != 0 && value2 != 0) {
                resultService.setText(msg1);
                resultQ1.setText(value1 + "");
                resultQ2.setText(value2 + "");
            }else{
                resultService.setText("Information");
                resultQ1.setText(".......");
                resultQ2.setText(".......");

            }

            Spinner editTextSample = getView().findViewById(R.id.spinner_scann);
            SharedPreferences prefs = getContext().getSharedPreferences("prefs_name", Context.MODE_PRIVATE);
            int spinnerIndx = prefs.getInt("spinner_indx", 0);
            editTextSample.setSelection(spinnerIndx);

            if (editTextSample.getSelectedItem().toString().equals("None selected !")) {
                gauge1.setValue(0);
                gauge1.setPointStartColor(Color.rgb(220, 220, 220));
                gauge1.setPointEndColor(Color.rgb(220, 220, 220));
                gauge2.setValue(0);
                gauge2.setPointStartColor(Color.rgb(220, 220, 220));
                gauge2.setPointEndColor(Color.rgb(220, 220, 220));
            } else {
                setGaugeColor(value1, value2);
            }


        } else {
            info.setBackgroundResource(R.drawable.rounded_edittext4);
            info.setText("Disconnected");
            resultService.setText("Information");
            gauge1.setValue(0);
            gauge1.setPointStartColor(Color.rgb(220, 220, 220));
            gauge1.setPointEndColor(Color.rgb(220, 220, 220));
            gauge2.setValue(0);
            gauge2.setPointStartColor(Color.rgb(220, 220, 220));
            gauge2.setPointEndColor(Color.rgb(220, 220, 220));
        }
        //un-register BroadcastReceiver

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            makeSpinnerScanning(getView());
        }
    }

    public class MyBroadcastReceiver_Update extends BroadcastReceiver {
        Fragment fragment;

        public MyBroadcastReceiver_Update(Fragment fragment) {
            this.fragment = fragment;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            update = intent.getStringExtra(MyService.EXTRA_KEY_UPDATE);
            if (update.equals("Connected")) {
                info.setBackgroundResource(R.drawable.rounded_edittext5);
                info.setText("Connected");
            } else {
                info.setBackgroundResource(R.drawable.rounded_edittext4);
                info.setText("Disconnected");
            }

        }
    }

    public class MyBroadcastReceiver1 extends BroadcastReceiver {
        Fragment fragment;

        public MyBroadcastReceiver1(Fragment fragment) {
            this.fragment = fragment;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String result1 = intent.getStringExtra(MyService.EXTRA_KEY_OUT1);
            String[] segs = result1.split("/");
            msg1 = "Raw Values:  V1 :  " + segs[0] + "    V2 :  " + segs[1] + "    V3 :  " + segs[2] + "    AMP :  " + segs[3];
            resultService.setText(msg1);
            value1 = (int) (Double.parseDouble(segs[0]) * 200);
            value2 = (int) (Double.parseDouble(segs[1]) * 200);
            setGaugeColor(value1, value2);
            resultQ1.setText(value1 + "");
            resultQ2.setText(value2 + "");
            info.setBackgroundResource(R.drawable.rounded_edittext5);
            info.setText("Connected");
        }
    }

    public class MyBroadcastReceiver2 extends BroadcastReceiver {
        Fragment fragment;

        public MyBroadcastReceiver2(Fragment fragment) {
            this.fragment = fragment;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String result2 = intent.getStringExtra(MyService.EXTRA_KEY_OUT2);
            String[] segs = result2.split("/");
            if (segs[0].equals("RED")) {

                resultQ1.setText("");
                resultQ2.setText("");
                resultService.setTextColor(Color.RED);

            } else if (segs[0].equals("BLACK")) {

                resultService.setTextColor(Color.BLACK);
                resultQ1.setText("Scanning...");
                resultQ2.setText("Scanning...");
                setGaugeColor((int) (0), (int) (0));
            }
            resultService.setText(segs[1]);

        }
    }

}


