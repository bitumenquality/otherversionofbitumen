package com.example.first.bitumenquality.subPages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.example.first.bitumenquality.ListViewAdapter;
import com.example.first.bitumenquality.R;
import com.example.first.bitumenquality.dataAccessObject.AlterungszustandDAO;
import com.example.first.bitumenquality.databaseClasses.Alterungszustand;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.first.bitumenquality.subPages.SubPageScanning.LOG_TAG;

public class SubPageScannedData extends Fragment {

    private AlterungszustandDAO dataAlterungszustand;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_page_scanned_data, container, false);
        showLieferungListEntries(rootView);
        initializeContextualActionBarAlterung(rootView);
        return rootView;
    }

    private void showLieferungListEntries(View rootView) {

        dataAlterungszustand = new AlterungszustandDAO(getContext());
        dataAlterungszustand.open();
        showAllListEntriesAlterung(rootView);
        dataAlterungszustand.close();
    }

    private void showAllListEntriesAlterung(View rootView) {
        List<Alterungszustand> memoList = dataAlterungszustand.getAllAlterungzustand();

        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < memoList.size(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            Alterungszustand alterungszustand = memoList.get(i);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            map.put("FIRST_COLUMN", alterungszustand.getProbenId() + "\t(" + alterungszustand.getBezeichnung()+")");
            map.put("SECOND_COLUMN", "   " + df.format(alterungszustand.getDate()));
            map.put("THIRD_COLUMN", "   " + alterungszustand.getMessungsfaktoren());
            map.put("FOURTH_COLUMN", " " + alterungszustand.getMessung());
            mylist.add(map);
        }
        Log.d(LOG_TAG, " count mylist: : " + mylist.size());
        //TODO make an base adapter
        ListView memosListView = (ListView) rootView.findViewById(R.id.lv_scanneddata);
        ListViewAdapter adapter = new ListViewAdapter(getActivity(), mylist);
        memosListView.setAdapter(adapter);

    }

    private void initializeContextualActionBarAlterung(final View rootView) {

        final ListView listView = (ListView) rootView.findViewById(R.id.lv_scanneddata);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                getActivity().getMenuInflater().inflate(R.menu.menu_contextual_action_bar, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.cab_delete:
                        SparseBooleanArray touchedShoppingMemosPositions = listView.getCheckedItemPositions();
                        for (int i = 0; i < touchedShoppingMemosPositions.size(); i++) {
                            boolean isChecked = touchedShoppingMemosPositions.valueAt(i);
                            if (isChecked) {
                                int postitionInListView = touchedShoppingMemosPositions.keyAt(i);
                                HashMap<Integer, Object> temp = (HashMap<Integer, Object>) listView.getItemAtPosition(postitionInListView);

                                dataAlterungszustand.open();
                                dataAlterungszustand.deleteAlterungszustand(dataAlterungszustand.getAllAlterungzustand().get(postitionInListView));
                                dataAlterungszustand.close();

                            }
                        }
                        showLieferungListEntries(rootView);
                        mode.finish();
                        return true;

                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            showLieferungListEntries(getView());
        }
    }

}
