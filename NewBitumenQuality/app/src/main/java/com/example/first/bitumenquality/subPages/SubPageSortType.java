package com.example.first.bitumenquality.subPages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.first.bitumenquality.R;
import com.example.first.bitumenquality.dataAccessObject.SorteDAO;
import com.example.first.bitumenquality.databaseClasses.Sorte;
import com.example.first.bitumenquality.ListViewAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class SubPageSortType extends Fragment {

    private SorteDAO dataSource6;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_page_sort_type, container, false);

        showSorteListEntries(rootView);
        activateAddButtonSorte(rootView);
        initializeContextualActionBarSorte(rootView);

        return rootView;
    }

    public void showAllListEntriesSorte(View rootview) {

        List<Sorte> memoList = dataSource6.getAllSorte();

        ArrayList<HashMap<String, String>> mylist = new ArrayList<>();
        for (int i = 0; i < memoList.size(); i++) {
            HashMap<String, String> map = new HashMap<>();
            Sorte sorte = memoList.get(i);

            map.put("FIRST_COLUMN", " " + sorte.getBezeichnung());
            map.put("SECOND_COLUMN", " " + sorte.getBeschreibung());
            map.put("THIRD_COLUMN", "Q1  " + "[ " + sorte.getQ1MinGreenQ() + " - " + sorte.getQ1Min() + " ]");
            map.put("FOURTH_COLUMN", "Q2  " + "[ " + sorte.getQ2MinGreenQ() + " - " + sorte.getQ2Min() + " ]");
            mylist.add(map);
        }


        Log.d("MainActivity", "count mylist:" + mylist.size());
        //TODO make an base adapter

        ListView memosListView = rootview.findViewById(R.id.lv_sorttype);
        ListViewAdapter adapter = new ListViewAdapter(getActivity(), mylist);
        memosListView.setAdapter(adapter);

    }

    private void showSorteListEntries(View rootView) {

        dataSource6 = new SorteDAO(getContext());
        dataSource6.open();
        showAllListEntriesSorte(rootView);
        dataSource6.close();
    }


    private void activateAddButtonSorte(final View rootview) {


        Button buttonAddSort = rootview.findViewById(R.id.btn_sorttype_save);
        final EditText editTextSInfo = rootview.findViewById(R.id.et_sorttype_beschreibung);
        final EditText editTextSName =  rootview.findViewById(R.id.et_sorttype_bezeichnung);
        final EditText editTextSQ1MinGreen =  rootview.findViewById(R.id.et_sorttype_q1mingreen);
        final EditText editTextSQ1MinYellow = rootview.findViewById(R.id.et_sorttype_q1minyellow);
        final EditText editTextSQ2MinGreen =  rootview.findViewById(R.id.et_sorttype_q2mingreen);
        final EditText editTextSQ2MinYellow = rootview.findViewById(R.id.et_sorttype_q2minyellow);


        buttonAddSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String name = editTextSName.getText().toString();
                editTextSName.setImeOptions(EditorInfo.IME_ACTION_DONE);
                String info = editTextSInfo.getText().toString();
                editTextSInfo.setImeOptions(EditorInfo.IME_ACTION_DONE);

                String q1Green = editTextSQ1MinGreen.getText().toString();
                String q1Yellow = editTextSQ1MinYellow.getText().toString();
                String q2Green = editTextSQ2MinGreen.getText().toString();
                String q2Yellow = editTextSQ2MinYellow.getText().toString();


                if (TextUtils.isEmpty(name)) {
                    editTextSName.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(info) ) {
                    editTextSInfo.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(q1Green)) {
                    editTextSQ1MinGreen.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(q1Yellow)) {
                    editTextSQ1MinYellow.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(q2Green)) {
                    editTextSQ2MinGreen.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(q2Yellow)) {
                    editTextSQ2MinYellow.setError(getString(R.string.output_errorMessage));
                    return;
                }

                boolean q1GreenIntegerBool = TextUtils.isDigitsOnly(editTextSQ1MinGreen.getText());
                boolean q1YellowIntegerBool = TextUtils.isDigitsOnly(editTextSQ1MinYellow.getText());
                boolean q2GreenIntegerBool = TextUtils.isDigitsOnly(editTextSQ2MinGreen.getText());
                boolean q2YellowIntegerBool = TextUtils.isDigitsOnly(editTextSQ2MinYellow.getText());

                if(!q1GreenIntegerBool){
                    editTextSQ1MinGreen.setError("value must be a number!");
                    return;
                }
                if(!q1YellowIntegerBool){
                    editTextSQ1MinYellow.setError("value must be a number!");
                    return;
                }
                if(!q2GreenIntegerBool){
                    editTextSQ2MinGreen.setError("value must be a number!");
                    return;
                }
                if(!q2YellowIntegerBool){
                    editTextSQ2MinYellow.setError("value must be a number!");
                    return;
                }



                dataSource6.open();
                Log.d("MainActivity", "parse int: Q1 and q2:" + Integer.parseInt(q1Green) + " " + Integer.parseInt(q1Yellow) +
                        Integer.parseInt(q2Green) + " " + Integer.parseInt(q2Yellow));

                dataSource6.createSorte(name, info, Integer.parseInt(q1Green), Integer.parseInt(q1Yellow),
                        Integer.parseInt(q2Green), Integer.parseInt(q2Yellow));
                dataSource6.close();

                InputMethodManager inputMethodManager;
                inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null) {
                    assert inputMethodManager != null;
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                showSorteListEntries(rootview);


            }
        });


    }

    private void initializeContextualActionBarSorte(final View rootview) {

        final ListView listView = rootview.findViewById(R.id.lv_sorttype);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                AppCompatActivity a = (AppCompatActivity) getActivity();
                a.getMenuInflater().inflate(R.menu.menu_contextual_action_bar, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.cab_delete:
                        SparseBooleanArray touchedShoppingMemosPositions = listView.getCheckedItemPositions();
                        for (int i = 0; i < touchedShoppingMemosPositions.size(); i++) {
                            boolean isChecked = touchedShoppingMemosPositions.valueAt(i);
                            if (isChecked) {
                                int postitionInListView = touchedShoppingMemosPositions.keyAt(i);
                               // HashMap<Integer, Object> temp = (HashMap<Integer, Object>) listView.getItemAtPosition(postitionInListView);
                                dataSource6.open();
                                dataSource6.deleteSorte(dataSource6.getAllSorte().get(postitionInListView));
                                dataSource6.close();

                            }
                        }
                        showSorteListEntries(rootview);


                        mode.finish();
                        return true;

                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

}
