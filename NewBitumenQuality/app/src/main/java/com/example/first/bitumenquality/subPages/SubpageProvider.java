package com.example.first.bitumenquality.subPages;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.first.bitumenquality.ListViewAdapter;
import com.example.first.bitumenquality.R;
import com.example.first.bitumenquality.dataAccessObject.HerstellerDAO;
import com.example.first.bitumenquality.dataAccessObject.SorteDAO;
import com.example.first.bitumenquality.databaseClasses.Hersteller;
import com.example.first.bitumenquality.databaseClasses.Sorte;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class SubpageProvider extends Fragment {

    private final static String TAG = "PROVIDER";
    private HerstellerDAO dataHersteller;
    private SorteDAO dataSource6 = new SorteDAO(getContext());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_page_provider, container, false);
        showHerstellerListEntries(rootView);

        /*Bundle bundle = getArguments();
        if(bundle != null){
           String name = bundle.getString("key");
            Toast.makeText(getActivity(),name,Toast.LENGTH_LONG).show();
        }else
            Toast.makeText(getActivity(),"ERROR",Toast.LENGTH_LONG).show();*/

        makeSpinnerHersteller(rootView);
        activateAddButtonHersteller(rootView);
        initializeContextualActionBarHersteller(rootView);

        return rootView;
    }

    private void makeSpinnerHersteller(View rootView) {

        Spinner My_spinner_Sample = (Spinner) rootView.findViewById(R.id.spinner_provider);
        ArrayAdapter<String> my_Adapter_Sample = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item, getTableValuesSorte());
        my_Adapter_Sample.add("None selected !");
        my_Adapter_Sample.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        My_spinner_Sample.setAdapter(my_Adapter_Sample);

    }


    public ArrayList<String> getTableValuesSorte() {
        ArrayList<String> my_array = new ArrayList<String>();
        try {
            dataSource6.open();

            List<Sorte> list = dataSource6.getAllSorte();

            for (int i = 0; i < list.size(); i++) {
                String NAME = list.get(i).getBezeichnung();
                my_array.add(NAME);
            }
            dataSource6.close();

        } catch (Exception e) {
            Toast.makeText(getActivity().getApplicationContext(), "Error encountered.",
                    Toast.LENGTH_LONG);
        }
        return my_array;
    }

    private void showAllListEntriesHersteller(View rootview) {

        List<Hersteller> memoList = dataHersteller.getAllHersteller();

        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        dataSource6 = new SorteDAO(getContext());
        dataSource6.open();
        List<Sorte> list = dataSource6.getAllSorte();
        dataSource6.close();
        String SorteName = "Empty !!!";
        for (int i = 0; i < memoList.size(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            Hersteller hersteller = memoList.get(i);

            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getId() == hersteller.getSortenId()) {
                    SorteName = list.get(j).getBezeichnung();
                    break;
                }
            }
            map.put("FIRST_COLUMN", " " + SorteName);
            map.put("SECOND_COLUMN", " " + hersteller.getName());
            map.put("THIRD_COLUMN", " " + hersteller.getBeschreibung());
            map.put("FOURTH_COLUMN", " ");
            mylist.add(map);
        }

        //TODO make an base adapter

        ListView memosListView = (ListView) rootview.findViewById(R.id.lv_provider);
        ListViewAdapter adapter = new ListViewAdapter(getActivity(), mylist);
        memosListView.setAdapter(adapter);

    }

    private void showHerstellerListEntries(View rootView) {

        dataHersteller = new HerstellerDAO(getContext());
        dataHersteller.open();
        showAllListEntriesHersteller(rootView);
        dataHersteller.close();


    }

    private void activateAddButtonHersteller(final View rootview) {

        Button buttonAddProvider = (Button) rootview.findViewById(R.id.btn_provider_save);
        final EditText editTextPName = (EditText) rootview.findViewById(R.id.et_provider_name);
        final EditText editTextPInfo = (EditText) rootview.findViewById(R.id.et_provider_beschreibung);
        final Spinner editTextPSorte = (Spinner) rootview.findViewById(R.id.spinner_provider);


        buttonAddProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = editTextPName.getText().toString();
                String info = editTextPInfo.getText().toString();
                String herstellerSorte = editTextPSorte.getSelectedItem().toString();


                if (TextUtils.isEmpty(name)) {
                    editTextPName.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(info)) {
                    editTextPInfo.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (editTextPSorte.getSelectedItem().toString().equals("None selected !")) {
                    TextView errorText = (TextView)editTextPSorte.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("no one is selected");//changes the selected item text to this
                    return;
                }

                dataSource6.open();
                Long id = dataSource6.getAllSorte(herstellerSorte).get(0).getId();
                dataSource6.close();
                dataHersteller.open();
                dataHersteller.createHersteller(id, name, info);
                dataHersteller.close();

                InputMethodManager inputMethodManager;
                inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                showHerstellerListEntries(rootview);
            }
        });

    }


    private void initializeContextualActionBarHersteller(final View rootview) {

        final ListView listView = (ListView) rootview.findViewById(R.id.lv_provider);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                AppCompatActivity a = (AppCompatActivity) getActivity();
                a.getMenuInflater().inflate(R.menu.menu_contextual_action_bar, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.cab_delete:
                        SparseBooleanArray touchedShoppingMemosPositions = listView.getCheckedItemPositions();
                        for (int i = 0; i < touchedShoppingMemosPositions.size(); i++) {
                            boolean isChecked = touchedShoppingMemosPositions.valueAt(i);
                            if (isChecked) {
                                int postitionInListView = touchedShoppingMemosPositions.keyAt(i);
                                HashMap<Integer, Object> temp = (HashMap<Integer, Object>) listView.getItemAtPosition(postitionInListView);
                                dataHersteller.open();
                                dataHersteller.deleteHersteller(dataHersteller.getAllHersteller().get(postitionInListView));
                                dataHersteller.close();

                            }
                        }
                        showHerstellerListEntries(rootview);

                        mode.finish();
                        return true;

                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            makeSpinnerHersteller(getView());
        }
    }

}
