package com.example.first.bitumenquality.subPages;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.first.bitumenquality.ListViewAdapter;
import com.example.first.bitumenquality.R;
import com.example.first.bitumenquality.dataAccessObject.LieferungDAO;
import com.example.first.bitumenquality.dataAccessObject.ProbeDAO;
import com.example.first.bitumenquality.databaseClasses.Lieferung;
import com.example.first.bitumenquality.databaseClasses.Probe;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.example.first.bitumenquality.subPages.SubPageScanning.LOG_TAG;

public class SubPageSampleProperties extends Fragment {

    private ProbeDAO dataProbe;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_page_sample_properties, container, false);

        showLieferungListEntries(rootView);
        makeSpinnerProbe(rootView);
        activateAddButton(rootView);
        initializeContextualActionBarProbe(rootView);

        return rootView;
    }

    private void makeSpinnerProbe(View rootView) {

        Spinner My_spinner_Sample = (Spinner) rootView.findViewById(R.id.spinner_sampleproperties);
        ArrayAdapter<String> my_Adapter_Sample = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item, getTableValuesLieferung());
        my_Adapter_Sample.add("None selected !");
        my_Adapter_Sample.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        My_spinner_Sample.setAdapter(my_Adapter_Sample);

    }


    private void initializeContextualActionBarProbe(final View rootView) {

        final ListView listView = (ListView) rootView.findViewById(R.id.lv_sampleproperties);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                getActivity().getMenuInflater().inflate(R.menu.menu_contextual_action_bar, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.cab_delete:
                        SparseBooleanArray touchedShoppingMemosPositions = listView.getCheckedItemPositions();
                        for (int i = 0; i < touchedShoppingMemosPositions.size(); i++) {
                            boolean isChecked = touchedShoppingMemosPositions.valueAt(i);
                            if (isChecked) {
                                int postitionInListView = touchedShoppingMemosPositions.keyAt(i);
                                HashMap<Integer, Object> temp = (HashMap<Integer, Object>) listView.getItemAtPosition(postitionInListView);
                                Log.d(LOG_TAG, "Position im ListView: " + postitionInListView + " Inhalt: " + temp.toString() + temp.size());
                                dataProbe.open();
                                dataProbe.deleteProbe(dataProbe.getAllProbe().get(postitionInListView));
                                dataProbe.close();

                            }
                        }
                        showLieferungListEntries(rootView);
                        mode.finish();
                        return true;

                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    private void showLieferungListEntries(View rootView) {

        dataProbe = new ProbeDAO(getContext());
        dataProbe.open();
        showAllListEntriesSampel(rootView);
        dataProbe.close();
    }


    private void showAllListEntriesSampel(View rootView) {

        List<Probe> memoList = dataProbe.getAllProbe();

        LieferungDAO dataLieferung = new LieferungDAO(getContext());
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        dataLieferung.open();
        List<Lieferung> list = dataLieferung.getAllLieferung();
        dataLieferung.close();
        String lieferungName = "Empty !!!";
        for (int i = 0; i < memoList.size(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            Probe probe = memoList.get(i);

            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getId() == probe.getLieferungId()) {
                    lieferungName = list.get(j).getBezeichnung();
                    break;
                }
            }
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            map.put("FIRST_COLUMN", lieferungName);
            map.put("SECOND_COLUMN", " " + probe.getBezeichnung());
            map.put("THIRD_COLUMN", " " + probe.getBeschreibung());
            map.put("FOURTH_COLUMN", " " + df.format(probe.getDate()));
            mylist.add(map);
        }

        Log.d(LOG_TAG, " count mylist: : " + mylist.size());
        //TODO make an base adapter

        ListView memosListView = (ListView) rootView.findViewById(R.id.lv_sampleproperties);
        ListViewAdapter adapter = new ListViewAdapter(getActivity(), mylist);
        memosListView.setAdapter(adapter);

    }

    private void activateAddButton(final View rootView) {

        Button buttonAddProductSample = (Button) rootView.findViewById(R.id.btn_sampleproperties_save);
        final EditText editTextInfo = (EditText) rootView.findViewById(R.id.et_sampleproperties_beschreibung);
        final EditText editTextName = (EditText) rootView.findViewById(R.id.et_sampleproperties_name);

        final EditText editTextDay = (EditText) rootView.findViewById(R.id.et_day_sampleproperties);
        final EditText editTextMonth = (EditText) rootView.findViewById(R.id.et_month_sampleproperties);
        final EditText editTextYear = (EditText) rootView.findViewById(R.id.et_year_sampleproperties);

        final Spinner editTextLieferung = (Spinner) rootView.findViewById(R.id.spinner_sampleproperties);


        buttonAddProductSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String day = editTextDay.getText().toString();
                String month = editTextMonth.getText().toString();
                String year = editTextYear.getText().toString();

                String name = editTextName.getText().toString();
                String info = editTextInfo.getText().toString();
                String lieferung = editTextLieferung.getSelectedItem().toString();

                if (TextUtils.isEmpty(day)) {
                    editTextDay.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(month)) {
                    editTextMonth.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(year)) {
                    editTextYear.setError(getString(R.string.output_errorMessage));
                    return;
                }

                if (TextUtils.isEmpty(name)) {
                    editTextName.setError(getString(R.string.output_errorMessage));
                    return;
                }

                if (TextUtils.isEmpty(info)) {
                    editTextInfo.setError(getString(R.string.output_errorMessage));
                    return;
                }
                if (editTextLieferung.getSelectedItem().toString().equals("None selected !")) {
                    TextView errorText = (TextView) editTextLieferung.getSelectedView();
                    errorText.setError("");
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    errorText.setText("no one is selected");//changes the selected item text to this
                    return;
                }
                boolean dayBool = TextUtils.isDigitsOnly(editTextDay.getText());
                boolean monthBool = TextUtils.isDigitsOnly(editTextMonth.getText());
                boolean yearBool = TextUtils.isDigitsOnly(editTextYear.getText());

                if (!dayBool || Integer.parseInt(day) < 1 || Integer.parseInt(day) > 31) {
                    editTextDay.setError("value problem!");
                    return;
                }
                if (!monthBool || Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {
                    editTextMonth.setError("value problem!");
                    return;
                }
                if (!yearBool || Integer.parseInt(year) < 1000 || Integer.parseInt(year) > 3000) {
                    editTextYear.setError("value problem!");
                    return;
                }


                dataProbe.open();
                LieferungDAO dataLieferung = new LieferungDAO(getContext());
                dataLieferung.open();

                Log.d(LOG_TAG, lieferung);
                Log.d(LOG_TAG, dataLieferung.getAllLieferung(lieferung).toString());

                Long id = dataLieferung.getAllLieferung(lieferung).get(0).getId();
                String newDate = String.format("%04d-%02d-%02d", Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
                Date dTemp = Date.valueOf(newDate);
                dataProbe.createProbe(
                        id, dTemp, name, info);
                dataLieferung.close();
                dataProbe.close();

                InputMethodManager inputMethodManager;
                inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                showLieferungListEntries(rootView);

            }
        });

    }

    public ArrayList<String> getTableValuesLieferung() {

        LieferungDAO dataLieferung = new LieferungDAO(getContext());

        ArrayList<String> my_array = new ArrayList<String>();
        try {
            dataLieferung.open();

            List<Lieferung> list = dataLieferung.getAllLieferung();

            for (int i = 0; i < list.size(); i++) {
                String NAME = list.get(i).getBezeichnung();
                my_array.add(NAME);
            }

            dataLieferung.close();

        } catch (Exception e) {
            Toast.makeText(getContext().getApplicationContext(), "Error encountered.",
                    Toast.LENGTH_LONG);
        }
        return my_array;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            makeSpinnerProbe(getView());
        }
    }
}
