package com.example.first.bitumenquality;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


import java.nio.ByteBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyService extends IntentService {


    static final String ACTION_USB_PERMISSION = "com.example.erfan.bitumen_quality";
    UsbManager usbManager;
    UsbDevice usbDevice = null;
    UsbInterface usbCdcInterface = null;
    UsbInterface usbHidInterface = null;
    UsbEndpoint usbCdcRead = null;
    UsbEndpoint usbCdcWrite = null;
    UsbDeviceConnection usbCdcConnection;
    Thread readThread = null;
    volatile boolean readThreadRunning = true;
    PendingIntent permissionIntent;
    Context context;


    StringBuilder receiveddata;

    Handler mHandler;

    //send data
    public static final String ACTION_MyIntentService1 = "com.example.androidintentservice.RESPONSE1";
    public static final String ACTION_MyIntentService2 = "com.example.androidintentservice.RESPONSE2";
    public static final String ACTION_MyUpdate = "com.example.androidintentservice.UPDATE";
    // public static final String EXTRA_KEY_IN = "EXTRA_IN";
    public static final String EXTRA_KEY_OUT1 = "EXTRA_OUT1";
    public static final String EXTRA_KEY_OUT2 = "EXTRA_OUT2";
    public static final String EXTRA_KEY_UPDATE = "EXTRA_UPDATE";
    String msgFromActivity;
    String extraOut;

    public MyService() {
        super("myService");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        //mHandler = new Handler(Looper.getMainLooper());
        mHandler = new Handler();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        Toast.makeText(getApplicationContext(), "Start " + MyService.class.getSimpleName(), Toast.LENGTH_LONG).show();
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "stop " + MyService.class.getSimpleName(), Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        receiveddata = new StringBuilder();
        Log.d("MainActivity", "Started the usb linstener");
        ByteBuffer buffer = ByteBuffer.allocate(255);

        UsbRequest request = new UsbRequest();
        usbCommunicationManager(getApplicationContext());
        connect();


        while (usbCdcRead == null) {
        }
        String checkConnect = "Connected";

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        request.initialize(usbCdcConnection, usbCdcRead);

        //  char dataByte, data ;
        int packetState = 0;
        while (readThreadRunning) {

            //send update
            Intent intentUpdate = new Intent();
            intentUpdate.setAction(ACTION_MyUpdate);
            intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
            intentUpdate.putExtra(EXTRA_KEY_UPDATE, checkConnect);
            sendBroadcast(intentUpdate);


            request.queue(buffer, buffer.capacity());
            // wait for status event
            //Log.d("MainActivity", " run.request: " + request.toString());

            if (usbCdcConnection.requestWait() == request) {

                // there is no way to know how many bytes are coming, so simply forward the non-null values

                for (int i = 0; i < buffer.capacity() && buffer.get(i) != 0; i++) {
                    //  Log.d("MainActivity", " run.request: " + "/" + (char)buffer.get(i) +"/" + " receiveddata: " + receiveddata.length() + " capacity:" + buffer.capacity());
                    receiveddata.append((char) buffer.get(i));

                    if ((char) buffer.get(i) == '\n') {
                        Pattern pResult = Pattern.compile("RESULT: Q1: (.+) -- Q2: (.+); absolute values: (.+) -- (.+) -- (.+); AMP: (.+)");
                        Pattern pOther = Pattern.compile("(INFO|ERROR): (.+)");

                        String msg = receiveddata.toString().trim();

                        final Matcher mResult = pResult.matcher(msg);
                        final Matcher mOther = pOther.matcher(msg);

                        if (mResult.find()) {
                            final float q1 = Float.parseFloat(mResult.group(1));
                            final float q2 = Float.parseFloat(mResult.group(2));
                            final float v1 = Float.parseFloat(mResult.group(3));
                            final float v2 = Float.parseFloat(mResult.group(4));
                            final float v3 = Float.parseFloat(mResult.group(5));
                            final float amp = Float.parseFloat(mResult.group(6));
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    String msg2 = v1 + "/" + v2 + "/" + v3 + "/" + amp;


                                    //return result
                                    Intent intentResponse = new Intent();
                                    intentResponse.setAction(ACTION_MyIntentService1);
                                    intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
                                    intentResponse.putExtra(EXTRA_KEY_OUT1, msg2);
                                    sendBroadcast(intentResponse);


                                }
                            });

                        } else if (mOther.find()) {

                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    String errorTester = "";
                                    if ("ERROR".equals(mOther.group(1))) {
                                        errorTester = "RED";
                                    } else {
                                        errorTester = "BLACK";
                                    }

                                    //return result
                                    Intent intentResponse = new Intent();
                                    intentResponse.setAction(ACTION_MyIntentService2);
                                    intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
                                    intentResponse.putExtra(EXTRA_KEY_OUT2, errorTester + "/" + mOther.group(2));
                                    sendBroadcast(intentResponse);

                                }
                            });

                        }
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }


                        receiveddata.delete(0, receiveddata.length() - 1);

                        break;
                    }

                }

            } else {
                Log.e("MainActivity", "Was not able to read from USB device, ending listening thread");
                readThreadRunning = false;
                break;
            }
        }

    }

    public String connect() {
        Log.d("MainActivity", "Connect: Start");
        // check if there's a connected usb device
        if (usbManager.getDeviceList().isEmpty()) {
            Log.d("MainActivity", "No connected devices");
            return "No connected devices";
        }

        // get the first (only) connected device
        Toast.makeText(context, "USB connected", Toast.LENGTH_LONG).show();
        Log.d("MainActivity", "i got an USB connected");
        usbDevice = usbManager.getDeviceList().values().iterator().next();

        // user must approve of connection if not in the /res/usb_device_filter.xml file
        usbManager.requestPermission(usbDevice, permissionIntent);

        Log.d("MainActivity", "Connect: Ende");
        return "Scanning..";
    }

    public void usbCommunicationManager(Context context) {
        this.context = context;
        usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

        // ask permission from user to use the usb device
        permissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        context.registerReceiver(usbReceiver, filter);
    }

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.d("MainActivity", "im in BroadcastReceiver");

            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                // broadcast is like an interrupt and works asynchronously with the class, it must be synced just in case
                synchronized (this) {
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        // fetch all the endpoints
                        Log.d("MainActivity", "setupConnection");
                        setupConnection();

                        // open and claim interface
                        usbCdcConnection = usbManager.openDevice(usbDevice);
                        usbCdcConnection.claimInterface(usbCdcInterface, true);

                        // set dtr to true (ready to accept data)
                        usbCdcConnection.controlTransfer(0x21, 0x22, 0x1, 0, null, 0, 0);

                    } else {
                        Log.d("trebla", "Permission denied for USB device");
                    }
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                if (usbDevice != null) {
                    usbCdcConnection.releaseInterface(usbCdcInterface);
                    usbCdcConnection.close();
                    usbCdcConnection = null;
                    usbDevice = null;
                    Log.d("MainActivity", "USB connection closed");
                }
            }
        }
    };

    private void setupConnection() {

        // find the right interface
        for (int i = 0; i < usbDevice.getInterfaceCount(); i++) {
            Log.d("MainActivity", "check device: " + i);

            // communications device class (CDC) type device
            if (usbDevice.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_CDC_DATA) {
                usbCdcInterface = usbDevice.getInterface(i);

                // find the endpoints
                for (int j = 0; j < usbCdcInterface.getEndpointCount(); j++) {
                    Log.d("MainActivity", "check endpoint: " + j);
                    if (usbCdcInterface.getEndpoint(j).getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                        if (usbCdcInterface.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_OUT) {
                            // from host to device
                            usbCdcWrite = usbCdcInterface.getEndpoint(j);
                            Log.d("MainActivity", "DIR_OUT");

                        }

                        if (usbCdcInterface.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_IN) {
                            // from device to host
                            usbCdcRead = usbCdcInterface.getEndpoint(j);
                            Log.d("MainActivity", "DIR_in:" + usbCdcRead.toString());
                        }
                    }
                }
            }
        }
    }

    public void stop() {
        usbDevice = null;
        usbCdcInterface = null;
        usbHidInterface = null;
        usbCdcRead = null;
        usbCdcWrite = null;

        context.unregisterReceiver(usbReceiver);
    }

}

